// Copyright (C) 2019  Project Tsukurou
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Representations and arithmetic operations for locations of blocks and chunks.

use crate::state::chunk::{CHUNK_BITS, CHUNK_MASK};
use std::marker::PhantomData;
use std::ops;

/// An absolute location in three dimensions.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Absolute<Unit> {
    _unit: PhantomData<Unit>,

    /// The X component of the absolute location.
    pub x: i64,

    /// The Y component of the absolute location.
    pub y: i64,

    /// The Z component of the absolute location.
    pub z: i64,
}

impl<Unit> Absolute<Unit> {
    /// Creates a triple with the given components.
    pub fn new(x: i64, y: i64, z: i64) -> Absolute<Unit> {
        Absolute {
            _unit: PhantomData,
            x,
            y,
            z,
        }
    }
}

impl<Unit> ops::Add<Relative<Unit>> for Absolute<Unit> {
    type Output = Absolute<Unit>;

    fn add(self, rhs: Relative<Unit>) -> Absolute<Unit> {
        Absolute::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl<Unit> ops::Sub<Relative<Unit>> for Absolute<Unit> {
    type Output = Absolute<Unit>;

    fn sub(self, rhs: Relative<Unit>) -> Absolute<Unit> {
        Absolute::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl<Unit> ops::Sub for Absolute<Unit> {
    type Output = Relative<Unit>;

    fn sub(self, rhs: Absolute<Unit>) -> Relative<Unit> {
        Relative::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

/// A relationship between two absolute locations, representing their difference on each axis.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Relative<Unit> {
    _unit: PhantomData<Unit>,

    /// The difference on the X axis.
    pub x: i64,

    /// The difference on the Y axis.
    pub y: i64,

    /// The difference on the Z axis.
    pub z: i64,
}

impl<Unit> Relative<Unit> {
    /// Creates a triple with the given components.
    pub fn new(x: i64, y: i64, z: i64) -> Relative<Unit> {
        Relative {
            _unit: PhantomData,
            x,
            y,
            z,
        }
    }
}

impl<Unit> ops::Add for Relative<Unit> {
    type Output = Relative<Unit>;

    fn add(self, rhs: Relative<Unit>) -> Relative<Unit> {
        Relative::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl<Unit> ops::Sub for Relative<Unit> {
    type Output = Relative<Unit>;

    fn sub(self, rhs: Relative<Unit>) -> Relative<Unit> {
        Relative::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl<Unit> ops::Mul<i64> for Relative<Unit> {
    type Output = Relative<Unit>;

    fn mul(self, rhs: i64) -> Relative<Unit> {
        Relative::new(self.x * rhs, self.y * rhs, self.z * rhs)
    }
}

impl<Unit> ops::Div<i64> for Relative<Unit> {
    type Output = Relative<Unit>;

    fn div(self, rhs: i64) -> Relative<Unit> {
        Relative::new(self.x / rhs, self.y / rhs, self.z / rhs)
    }
}

impl<Unit> ops::Rem<i64> for Relative<Unit> {
    type Output = Relative<Unit>;

    fn rem(self, rhs: i64) -> Relative<Unit> {
        Relative::new(self.x % rhs, self.y % rhs, self.z % rhs)
    }
}

/// The unit equal to one block length.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BlockUnit;

/// The unit equal to one chunk length (16 block lengths).
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ChunkUnit;

/// An absolute location in units of block lengths.
pub type BlockAbsolute = Absolute<BlockUnit>;

/// A relative location in units of block lengths.
pub type BlockRelative = Relative<BlockUnit>;

/// An absolute location in units of chunk lengths.
pub type ChunkAbsolute = Absolute<ChunkUnit>;

/// A relative location in units of chunk lengths.
pub type ChunkRelative = Relative<ChunkUnit>;

impl BlockAbsolute {
    /// Returns the absolute location of the chunk to which this block belongs.
    pub fn parent_chunk(&self) -> ChunkAbsolute {
        Absolute::new(
            self.x >> CHUNK_BITS,
            self.y >> CHUNK_BITS,
            self.z >> CHUNK_BITS,
        )
    }

    /// Returns this block's chunk-local relative location within its parent chunk.
    pub fn chunk_local(&self) -> BlockRelative {
        Relative::new(
            self.x & CHUNK_MASK,
            self.y & CHUNK_MASK,
            self.z & CHUNK_MASK,
        )
    }
}

impl ChunkAbsolute {
    /// Resolves a chunk-local block location into an absolute location using this chunk.
    pub fn resolve_block(&self, chunk_local: &BlockRelative) -> BlockAbsolute {
        Absolute::new(
            (self.x << CHUNK_BITS) + chunk_local.x,
            (self.y << CHUNK_BITS) + chunk_local.y,
            (self.z << CHUNK_BITS) + chunk_local.z,
        )
    }
}
