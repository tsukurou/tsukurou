// Copyright (C) 2019  Project Tsukurou
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Associated items for `World`.

use crate::location::{BlockAbsolute, ChunkAbsolute};
use crate::state::block::{BlockType, TypeId};
use crate::state::{Block, Chunk};
use crate::table::Table;
use std::collections::HashMap;
use std::iter::FusedIterator;

/// A virtually infinite three-dimensional span of blocks, segmented into chunks and dynamically /
/// partially loaded as needed.
#[derive(Debug, Clone, PartialEq)]
pub struct World {
    chunks: HashMap<ChunkAbsolute, Chunk>,
    block_types: Vec<BlockType>,
    properties: Table,
}

impl World {
    /// Creates a new, empty world.
    pub fn new() -> World {
        World {
            chunks: HashMap::new(),
            block_types: Vec::new(),
            properties: Table::new(),
        }
    }

    /// References the block at the given location if its parent chunk is loaded.
    pub fn block(&self, location: &BlockAbsolute) -> Option<&Block> {
        self.chunk(&location.parent_chunk())
            .map(|chunk| unsafe { chunk.block_unchecked(&location.chunk_local()) })
    }

    /// Mutably references the block at the given location if its parent chunk is loaded.
    pub fn block_mut(&mut self, location: &BlockAbsolute) -> Option<&mut Block> {
        self.chunk_mut(&location.parent_chunk())
            .map(|chunk| unsafe { chunk.block_unchecked_mut(&location.chunk_local()) })
    }

    /// References the chunk at the given location if it is loaded.
    pub fn chunk(&self, location: &ChunkAbsolute) -> Option<&Chunk> {
        self.chunks.get(location)
    }

    /// Mutably references the chunk at the given location if it is loaded.
    pub fn chunk_mut(&mut self, location: &ChunkAbsolute) -> Option<&mut Chunk> {
        self.chunks.get_mut(location)
    }

    /// Returns an iterator with items `(ChunkAbsolute, &Chunk)` that visits each loaded chunk.
    pub fn chunks(&self) -> Chunks {
        Chunks::new(self)
    }

    /// Returns a mutating iterator with items `(ChunkAbsolute, &mut Chunk)` that visits each
    /// loaded chunk.
    pub fn chunks_mut(&mut self) -> ChunksMut {
        ChunksMut::new(self)
    }

    /// Loads the chunk into the given location, returning the chunk previously loaded there.
    pub fn load(&mut self, location: ChunkAbsolute, chunk: Chunk) -> Option<Chunk> {
        self.chunks.insert(location, chunk)
    }

    /// Attempts to unload the chunk from the given location, returning it if one was loaded.
    pub fn unload(&mut self, location: &ChunkAbsolute) -> Option<Chunk> {
        self.chunks.remove(location)
    }

    /// Unloads all loaded chunks and provides an iterator that returns ownership of them all to
    /// the caller.
    pub fn unload_all(&mut self) -> UnloadAll {
        UnloadAll::new(self)
    }

    /// Resolves the given type ID into a block type.
    pub fn block_type(&self, id: &TypeId) -> Option<&BlockType> {
        self.block_types.get(id.value)
    }

    /// Mutably references the block type corresponding to this type ID.
    pub fn block_type_mut(&mut self, id: &TypeId) -> Option<&mut BlockType> {
        self.block_types.get_mut(id.value)
    }

    /// Registers the given block type and assigns it an ID.
    pub fn register(&mut self, block_type: BlockType) -> TypeId {
        self.block_types.push(block_type);
        TypeId {
            value: self.block_types.len() - 1,
        }
    }

    /// References this world's properties table.
    pub fn properties(&self) -> &Table {
        &self.properties
    }

    /// Mutably references this world's properties table.
    pub fn properties_mut(&mut self) -> &mut Table {
        &mut self.properties
    }
}

/// An iterator over the loaded chunks in a world, created by `World::chunks_mut`.
#[derive(Debug, Clone)]
pub struct Chunks<'a> {
    iter: std::collections::hash_map::Iter<'a, ChunkAbsolute, Chunk>,
}

impl<'a> Chunks<'a> {
    fn new(world: &'a World) -> Chunks<'a> {
        Chunks {
            iter: world.chunks.iter(),
        }
    }
}

impl<'a> Iterator for Chunks<'a> {
    type Item = (ChunkAbsolute, &'a Chunk);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(&location, chunk)| (location, chunk))
    }
}

impl<'a> FusedIterator for Chunks<'a> {}

impl<'a> ExactSizeIterator for Chunks<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}

/// A mutating iterator over the loaded chunks in a world, created by `World::chunks_mut`.
#[derive(Debug)]
pub struct ChunksMut<'a> {
    iter: std::collections::hash_map::IterMut<'a, ChunkAbsolute, Chunk>,
}

impl<'a> ChunksMut<'a> {
    fn new(world: &'a mut World) -> ChunksMut<'a> {
        ChunksMut {
            iter: world.chunks.iter_mut(),
        }
    }
}

impl<'a> Iterator for ChunksMut<'a> {
    type Item = (ChunkAbsolute, &'a mut Chunk);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(&location, chunk)| (location, chunk))
    }
}

impl<'a> FusedIterator for ChunksMut<'a> {}

impl<'a> ExactSizeIterator for ChunksMut<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}

/// An iterator over a World's unloaded chunks, created by `World::unload_all()`.
#[derive(Debug)]
pub struct UnloadAll<'a> {
    iter: std::collections::hash_map::Drain<'a, ChunkAbsolute, Chunk>,
}

impl<'a> UnloadAll<'a> {
    fn new(world: &'a mut World) -> UnloadAll<'a> {
        UnloadAll {
            iter: world.chunks.drain(),
        }
    }
}

impl<'a> Iterator for UnloadAll<'a> {
    type Item = (ChunkAbsolute, Chunk);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<'a> FusedIterator for UnloadAll<'a> {}

impl<'a> ExactSizeIterator for UnloadAll<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}
