// Copyright (C) 2019  Project Tsukurou
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Associated items for `Block`.

use crate::table::Table;

/// The definition of a block type, used to template commonly-used sets of block properties so they
/// don't have to be stored multiple times.
#[derive(Debug, Clone, PartialEq)]
pub struct BlockType {
    base_properties: Table,
}

impl BlockType {
    /// Creates a block type with the given attributes.
    pub fn new(base_properties: Table) -> BlockType {
        BlockType { base_properties }
    }
}

/// A unique identifier for a block's type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TypeId {
    pub(crate) value: usize,
}

/// A cubic object, or "voxel", that is the basic building block of game state.
#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    type_id: TypeId,
    properties: Option<Box<Table>>,
}

impl Block {
    /// Creates a block with the given type and properties.
    pub fn new(type_id: TypeId, properties: Option<Table>) -> Block {
        Block {
            type_id,
            properties: properties.map(Box::new),
        }
    }

    /// References the block's type.
    pub fn type_id(&self) -> &TypeId {
        &self.type_id
    }

    /// Mutably references the block's type.
    pub fn type_id_mut(&mut self) -> &mut TypeId {
        &mut self.type_id
    }

    /// Checks if the block has a properties table.
    pub fn has_properties(&self) -> bool {
        self.properties.is_some()
    }

    /// Gets the block's properties table, creating it if it doesn't exist.
    pub fn make_properties(&mut self) -> &mut Table {
        self.properties.get_or_insert(Box::new(Table::new()))
    }

    /// References the block's table of properties if it exists.
    pub fn properties(&self) -> Option<&Table> {
        self.properties.as_ref().map(Box::as_ref)
    }

    /// Mutably references the block's table of properties if it exists.
    pub fn properties_mut(&mut self) -> Option<&mut Table> {
        self.properties.as_mut().map(Box::as_mut)
    }
}
