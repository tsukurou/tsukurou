// Copyright (C) 2019  Project Tsukurou
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Associated items for `Chunk`.

use crate::location::BlockRelative;
use crate::state::block::Block;
use crate::table::Table;
use std::iter::FusedIterator;

/// The number least significant bits reserved for a block's chunk-local coordinates in its
/// absolute world location.
pub const CHUNK_BITS: usize = 4;

/// The length of a chunk along one axis, measured in blocks (`2.pow(CHUNK_BITS)`).
pub const CHUNK_LEN: usize = 16;

/// The total number of blocks in a chunk (`CHUNK_LEN.pow(3)`).
pub const CHUNK_SIZE: usize = 4096;

/// A bitmask that retains the chunk-local coordinates of a block when combined with a block's
/// location using bitwise AND.
pub const CHUNK_MASK: i64 = 0xf;

/// Returns true if and only if _all_ of the components of the location are in the inclusive
/// range 0-15.
fn in_range(b: &BlockRelative) -> bool {
    (b.x | b.y | b.z) & !CHUNK_MASK == 0
}

/// Implements a one-to-one mapping between each location within a chunk's range (0-15 for each
/// component) and each index in the inclusive range 0-4095. Output is not well-defined for inputs
/// outside of the domain.
fn to_index(b: &BlockRelative) -> usize {
    (b.z as usize) << 2 * CHUNK_BITS | (b.y as usize) << CHUNK_BITS | (b.x as usize)
}

/// Performs the inverse mapping of `to_index`. Output is not well-defined for inputs outside of
/// the domain.
fn to_location(i: usize) -> BlockRelative {
    BlockRelative::new(
        i as i64 & CHUNK_MASK,
        (i as i64 >> CHUNK_BITS) & CHUNK_MASK,
        i as i64 >> 2 * CHUNK_BITS,
    )
}

/// A fixed-size collection of blocks, arranged in a 16x16x16 cube.
#[derive(Debug, Clone, PartialEq)]
pub struct Chunk {
    blocks: Box<[Block]>,
    properties: Table,
}

impl Chunk {
    /// References the block at the given location relative to this chunk, or `None` if the
    /// location is out of range.
    pub fn block(&self, location: &BlockRelative) -> Option<&Block> {
        if in_range(location) {
            Some(unsafe { self.block_unchecked(location) })
        } else {
            None
        }
    }

    /// Mutably references the block at the given location relative to this chunk, or `None` if
    /// the location is out of range.
    pub fn block_mut(&mut self, location: &BlockRelative) -> Option<&mut Block> {
        if in_range(location) {
            Some(unsafe { self.block_unchecked_mut(location) })
        } else {
            None
        }
    }

    /// References this chunk's properties table.
    pub fn properties(&self) -> &Table {
        &self.properties
    }

    /// Mutably references this chunk's properties table.
    pub fn properties_mut(&mut self) -> &mut Table {
        &mut self.properties
    }

    /// Returns an iterator with items `(BlockRelative, &Block)` that references each block in this
    /// chunk with its location, in no specific order.
    pub fn blocks(&self) -> Blocks {
        Blocks::new(self)
    }

    /// Returns an iterator with items `(BlockRelative, &mut Block)` that mutably references each
    /// block in this chunk with its location, in no specific order.
    pub fn blocks_mut(&mut self) -> BlocksMut {
        BlocksMut::new(self)
    }

    /// References the block at the given location relative to this chunk.
    ///
    /// # Safety
    ///
    /// This function does not check if the given location is within bounds. Undefined behavior
    /// may occur if _any_ of the components are outside the 0-15 range.
    pub unsafe fn block_unchecked(&self, location: &BlockRelative) -> &Block {
        self.blocks.get_unchecked(to_index(location))
    }

    /// Mutably references the block at the given location relative to this chunk.
    ///
    /// # Safety
    ///
    /// This function does not check if the given location is within bounds. Undefined behavior
    /// may occur if _any_ of the components are outside the 0-15 range.
    pub unsafe fn block_unchecked_mut(&mut self, location: &BlockRelative) -> &mut Block {
        self.blocks.get_unchecked_mut(to_index(location))
    }
}

/// An iterator over the blocks in a chunk, created by `Chunk::blocks`.
#[derive(Debug, Clone)]
pub struct Blocks<'a> {
    iter: std::iter::Enumerate<std::slice::Iter<'a, Block>>,
}

impl<'a> Blocks<'a> {
    fn new(chunk: &'a Chunk) -> Blocks<'a> {
        Blocks {
            iter: chunk.blocks.iter().enumerate(),
        }
    }
}

impl<'a> Iterator for Blocks<'a> {
    type Item = (BlockRelative, &'a Block);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(i, b)| (to_location(i), b))
    }
}

impl<'a> FusedIterator for Blocks<'a> {}

impl<'a> ExactSizeIterator for Blocks<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}

/// A mutating iterator over the blocks in a chunk, created by `Chunk::blocks_mut`.
#[derive(Debug)]
pub struct BlocksMut<'a> {
    iter: std::iter::Enumerate<std::slice::IterMut<'a, Block>>,
}

impl<'a> BlocksMut<'a> {
    fn new(chunk: &'a mut Chunk) -> BlocksMut<'a> {
        BlocksMut {
            iter: chunk.blocks.iter_mut().enumerate(),
        }
    }
}

impl<'a> Iterator for BlocksMut<'a> {
    type Item = (BlockRelative, &'a mut Block);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(i, b)| (to_location(i), b))
    }
}

impl<'a> FusedIterator for BlocksMut<'a> {}

impl<'a> ExactSizeIterator for BlocksMut<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}
