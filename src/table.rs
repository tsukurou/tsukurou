// Copyright (C) 2019  Project Tsukurou
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! The `Table` type for storing arbitrary data of any structure.

use std::collections::HashMap;
use std::iter::FusedIterator;

/// A table of named values.
#[derive(Debug, Clone, PartialEq)]
pub struct Table {
    map: HashMap<String, Value>,
}

impl Table {
    /// Creates a new, empty table.
    pub fn new() -> Table {
        Table {
            map: HashMap::new(),
        }
    }

    /// Returns `true` when there are no entries in the table.
    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    /// Removes all entries from the table.
    pub fn clear(&mut self) {
        self.map.clear()
    }

    /// Returns `true` if a value exists with the given name.
    pub fn contains(&self, name: &str) -> bool {
        self.map.contains_key(name)
    }

    /// Gets a reference to the value with the given name if it exists.
    pub fn get(&self, name: &str) -> Option<&Value> {
        self.map.get(name)
    }

    /// Gets a mutable reference to the value with the given name if it exists.
    pub fn get_mut(&mut self, name: &str) -> Option<&mut Value> {
        self.map.get_mut(name)
    }

    /// Adds a value with the given name, returning value previously assigned to that name if it
    /// exists.
    pub fn insert(&mut self, name: String, value: Value) -> Option<Value> {
        self.map.insert(name, value)
    }

    /// Removes the value with the given name, returning it if it exists.
    pub fn remove(&mut self, name: &str) -> Option<Value> {
        self.map.remove(name)
    }

    /// Returns an iterator over the entries in this table.
    pub fn iter(&self) -> Iter {
        Iter::new(self)
    }

    /// Returns a mutating iterator over the entries in this table.
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut::new(self)
    }
}

/// An iterator over table entries, created by `Table::iter()`.
#[derive(Debug, Clone)]
pub struct Iter<'a> {
    iter: std::collections::hash_map::Iter<'a, String, Value>,
}

impl<'a> Iter<'a> {
    fn new(table: &'a Table) -> Iter<'a> {
        Iter {
            iter: table.map.iter(),
        }
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = (&'a String, &'a Value);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<'a> FusedIterator for Iter<'a> {}

impl<'a> ExactSizeIterator for Iter<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}

/// A mutating iterator over table entries, created by `Table::iter_mut()`.
#[derive(Debug)]
pub struct IterMut<'a> {
    iter: std::collections::hash_map::IterMut<'a, String, Value>,
}

impl<'a> IterMut<'a> {
    fn new(table: &'a mut Table) -> IterMut<'a> {
        IterMut {
            iter: table.map.iter_mut(),
        }
    }
}

impl<'a> Iterator for IterMut<'a> {
    type Item = (&'a String, &'a mut Value);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<'a> FusedIterator for IterMut<'a> {}

impl<'a> ExactSizeIterator for IterMut<'a> {
    fn len(&self) -> usize {
        self.iter.len()
    }
}

/// A value with a dynamic type.
#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    #[allow(missing_docs)]
    Null,

    #[allow(missing_docs)]
    Bool(bool),

    #[allow(missing_docs)]
    Int(i64),

    #[allow(missing_docs)]
    Float(f64),

    #[allow(missing_docs)]
    Bytes(Vec<u8>),

    #[allow(missing_docs)]
    String(String),

    #[allow(missing_docs)]
    List(Vec<Value>),

    #[allow(missing_docs)]
    Table(Table),
}

impl Value {
    /// Returns true if this value is a `Null` variant.
    pub fn is_null(&self) -> bool {
        match self {
            Value::Null => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `Bool` variant.
    pub fn is_bool(&self) -> bool {
        match self {
            Value::Bool(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is an `Int` variant.
    pub fn is_int(&self) -> bool {
        match self {
            Value::Int(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `Float` variant.
    pub fn is_float(&self) -> bool {
        match self {
            Value::Float(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `Bytes` variant.
    pub fn is_bytes(&self) -> bool {
        match self {
            Value::Bytes(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `String` variant.
    pub fn is_string(&self) -> bool {
        match self {
            Value::String(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `List` variant.
    pub fn is_list(&self) -> bool {
        match self {
            Value::List(_) => true,
            _ => false,
        }
    }

    /// Returns true if this value is a `Table` variant.
    pub fn is_table(&self) -> bool {
        match self {
            Value::Table(_) => true,
            _ => false,
        }
    }

    /// References the inner `bool` value if this is a `Bool` variant.
    pub fn as_bool(&self) -> Option<&bool> {
        match self {
            Value::Bool(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `i64` value if this is an `Int` variant.
    pub fn as_int(&self) -> Option<&i64> {
        match self {
            Value::Int(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `f64` value if this is a `Float` variant.
    pub fn as_float(&self) -> Option<&f64> {
        match self {
            Value::Float(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Vec<u8>` value if this is a `Bytes` variant.
    pub fn as_bytes(&self) -> Option<&Vec<u8>> {
        match self {
            Value::Bytes(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `String` value if this is a `String` variant.
    pub fn as_string(&self) -> Option<&String> {
        match self {
            Value::String(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Vec<Value>` if this is a `List` variant.
    pub fn as_list(&self) -> Option<&Vec<Value>> {
        match self {
            Value::List(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Table` value if this is a `Table` variant.
    pub fn as_table(&self) -> Option<&Table> {
        match self {
            Value::Table(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `bool` value if this is a `Bool` variant.
    pub fn as_bool_mut(&mut self) -> Option<&mut bool> {
        match self {
            Value::Bool(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `i64` value if this is an `Int` variant.
    pub fn as_int_mut(&mut self) -> Option<&mut i64> {
        match self {
            Value::Int(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `f64` value if this is a `Float` variant.
    pub fn as_float_mut(&mut self) -> Option<&mut f64> {
        match self {
            Value::Float(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Vec<u8>` value if this is a `Bytes` variant.
    pub fn as_bytes_mut(&mut self) -> Option<&mut Vec<u8>> {
        match self {
            Value::Bytes(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `String` value if this is a `String` variant.
    pub fn as_string_mut(&mut self) -> Option<&mut String> {
        match self {
            Value::String(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Vec<Value>` if this is a `List` variant.
    pub fn as_list_mut(&mut self) -> Option<&mut Vec<Value>> {
        match self {
            Value::List(x) => Some(x),
            _ => None,
        }
    }

    /// References the inner `Table` value if this is a `Table` variant.
    pub fn as_table_mut(&mut self) -> Option<&mut Table> {
        match self {
            Value::Table(x) => Some(x),
            _ => None,
        }
    }

    /// Unwraps the inner `bool` value if this is a `Bool` variant,
    /// returning the original value otherwise.
    pub fn into_bool(self) -> Result<bool, Value> {
        match self {
            Value::Bool(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `i64` value if this is an `Int` variant,
    /// returning the original value otherwise.
    pub fn into_int(self) -> Result<i64, Value> {
        match self {
            Value::Int(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `f64` value if this is a `Float` variant,
    /// returning the original value otherwise.
    pub fn into_float(self) -> Result<f64, Value> {
        match self {
            Value::Float(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `Vec<u8>` value if this is a `Bytes` variant,
    /// returning the original value otherwise.
    pub fn into_bytes(self) -> Result<Vec<u8>, Value> {
        match self {
            Value::Bytes(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `String` value if this is a `String` variant,
    /// returning the original value otherwise.
    pub fn into_string(self) -> Result<String, Value> {
        match self {
            Value::String(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `Vec<Value>` value if this is a `List` variant,
    /// returning the original value otherwise.
    pub fn into_list(self) -> Result<Vec<Value>, Value> {
        match self {
            Value::List(x) => Ok(x),
            other => Err(other),
        }
    }

    /// Unwraps the inner `Table` value if this is a `Table` variant,
    /// returning the original value otherwise.
    pub fn into_table(self) -> Result<Table, Value> {
        match self {
            Value::Table(x) => Ok(x),
            other => Err(other),
        }
    }
}

impl From<bool> for Value {
    fn from(x: bool) -> Value {
        Value::Bool(x)
    }
}

impl From<i64> for Value {
    fn from(x: i64) -> Value {
        Value::Int(x)
    }
}

impl From<f64> for Value {
    fn from(x: f64) -> Value {
        Value::Float(x)
    }
}

impl From<Vec<u8>> for Value {
    fn from(x: Vec<u8>) -> Value {
        Value::Bytes(x)
    }
}

impl From<String> for Value {
    fn from(x: String) -> Value {
        Value::String(x)
    }
}

impl From<Vec<Value>> for Value {
    fn from(x: Vec<Value>) -> Value {
        Value::List(x)
    }
}

impl From<Table> for Value {
    fn from(x: Table) -> Value {
        Value::Table(x)
    }
}
